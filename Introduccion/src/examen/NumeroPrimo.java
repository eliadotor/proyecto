package examen;

import java.util.Scanner;

public class NumeroPrimo {
	public static void main(String[] args) {
		int numero = pedirNumero();
		boolean primo = esPrimo(numero);
		verResultado(numero, primo);
	}
	
	public static int pedirNumero() {
		Scanner entrada = new Scanner(System.in);
		System.out.println("Introduce un número");
		return entrada.nextInt();
	}
	
	public static boolean esPrimo(int numero) {
		boolean primo = false;
		boolean salir = false;
		do {
			for(int i = numero; i>1; i--){
				primo = numero % i-1 == 0;
				if(primo=true) {
					salir = true;
				}
			}
		}while(!salir);
		return true;
	}
	
	public static void verResultado(int numero, boolean primo) {
		if(primo!=false) {
			 System.out.println("El número " + numero + " es primo.");

		}else {
			 System.out.println("El número " + numero + " no es primo.");

		}
	}
}
