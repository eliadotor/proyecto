package programacionModular;

import java.util.Scanner;

public class FactorialRecursivo {

	public static void main(String[] args) {
			
			int numero = pedirNumero();
			int factorial = calcularFactorial(numero);
			verResultado(numero, factorial);
			
		}
	public static int pedirNumero() {
			Scanner entrada = new Scanner(System.in);
			System.out.println("Introduzca un número para calcular su factorial");
			return entrada.nextInt();
		}
			
	public static int calcularFactorial(int numero) {
			if(numero == 0) {
				return 1;
			}else {
				return numero * calcularFactorial(numero-1);
			}
		}
			
	public static void verResultado(int numero, int factorial) {
			System.out.println(numero + "! = " + factorial);
	
		}
			
			
	
}
