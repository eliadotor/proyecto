package programacionModular;

//Importamos la clase Scanner
import java.util.Scanner;
public class rectangulo
{
    /*Este programa introduce por teclado la base y la altura de un 
     * rectángulo y visualiza en pantalla su area y su perimetro*/

    public static void main(String args[])
    {
        double base = pedirBase();
        double altura = pedirAltura();
        double area = calcularArea(base, altura);
        double perimetro = calcularPerimetro(base, altura);
        verResultado(area, perimetro);
    }   
        
    ///////////////////////////////////////////////////////
    public static double pedirBase() {
    	Scanner entrada = new Scanner(System.in);
        //Introducimos por teclado la base del rectángulo
    	System.out.println("Introduce la base del rectángulo");
        double base = entrada.nextDouble();
        return base;
    }
    
    ///////////////////////////////////////////////////////
    
    
    public static double pedirAltura() {
    	Scanner entrada = new Scanner(System.in);
        //Introducimos por teclado la altura del rectángulo
    	System.out.println("Introduce la altura del rectángulo");
        return entrada.nextDouble();
       
    }
    
    ////////////////////////////////////////////////////////
    
    public static double calcularArea(double base, double altura) {
    	return base * altura;
    	
    }
    
	////////////////////////////////////////////////////////
	    
	public static double calcularPerimetro(double base, double altura) {
	return 2*(base + altura);

	}
	///////////////////////////////////////////////////////////
	public static void verResultado(double area, double perimetro){
		System.out.println("El area es " + area + " y el perimetro " + perimetro);
	}

}
