package programacionModular;

import java.util.Scanner;

public class salario {
	
	static Scanner entrada = new Scanner(System.in);

	public static void main(String[] args) {
		int trabajadores = pedirTrabajadores();
		double tarifa = pedirTarifa();
		calcularSalario(trabajadores, tarifa);
	}

	public static int pedirTrabajadores() {
		System.out.println("Introduce el número de trabajadores");
		return entrada.nextInt();
		
	}
	
	public static double pedirTarifa() {
		System.out.println("Introduce la tarifa ordinaria");
		double tarifa = entrada.nextDouble();
		boolean tarifaValida;
		do {
			tarifaValida = (tarifa > 0);
			if(!tarifaValida) {
				System.out.println("ERROR. La tarifa debe ser mayor que 0.");
			}
		}while(!tarifaValida);
		return tarifa;
		
	}

	public static void calcularSalario (int trabajadores, double tarifa) {
		
		for(int i = 1; i <= trabajadores; i++) {
			System.out.println("\nTrabajador " + i + ":");
			int horas = pedirHoras();
			double bruto = calcularSalarioBruto(horas, tarifa);
			double impuestos = calcularImpuestos(bruto);
			double neto = calcularNeto(bruto, impuestos);
			verResultado(bruto, impuestos, neto);
		}
		
	}
	public static int pedirHoras() {
		System.out.println("Introduce el número de horas");
		int horas = entrada.nextInt();
		return horas;
		
	}
	
	public static double calcularSalarioBruto(int horas, double tarifa){
		final int horasMinimas = 38;
		double bruto;
		int horasExtra = horas - horasMinimas;
		bruto = (tarifa*horasMinimas) + ((horasExtra*1.5)*tarifa);
		return bruto;
		
	}
	public static double calcularImpuestos(double bruto){
		final int salarioMinimo = 600;
		double impuestos = 0;
		if(bruto <= 600) {
			impuestos = 0;
		}else {
			if(bruto <= 1200 ) {
				impuestos = (bruto - salarioMinimo)*0.25;
			}else if(bruto <= 1800 ) {
				impuestos = (salarioMinimo*0.25) +(bruto - ((salarioMinimo)*2))*0.45;
			}
			
		}
		return impuestos;
		
	}
	
	public static double calcularNeto(double bruto, double impuestos) {
		double neto = bruto - impuestos;
		return neto;
	}
	
	
	public static void verResultado (double bruto, double impuestos, double neto){
		System.out.println("El salario bruto es " + bruto + ". Los impuestos son " + impuestos + ". Y el salario neto es " + neto);
	}
	

	
}
