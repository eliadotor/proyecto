package programacionModular;

import java.util.Scanner;

public class PotenciaRecursiva {
	public static void main(String[] args) {
			
			int base = pedirBase();
			int exp = pedirExponente();
			int potencia = calcularPotencia(base, exp);
			verResultado(base, exp, potencia);
		}
		
		public static int pedirBase(){
			Scanner entrada = new Scanner(System.in);
			System.out.println("Introduce un número");
			return entrada.nextInt();
		}
		
		public static int pedirExponente(){
			Scanner entrada = new Scanner(System.in);
			System.out.println("Introduce el exponente");
			return entrada.nextInt();
		}
		
		public static int calcularPotencia(int base, int exp) {
			if(exp == 0) {
				return 1;
			}else {
				return base * calcularPotencia(base, exp -1);
			}
			
		}
		
		public static void verResultado(int base, int exp, int potencia) {
			System.out.println("La potencia de " + base + " elevado a " + exp + " es " + potencia);
		}

}
