package programacionModular;

import java.util.Scanner;

public class combinaciones {

	public static void main(String[] args) {
		int m = pedirM();
		int n = pedirN(m);
		int combinacion = calcularCombinacion(m, n);
		verResultado(m, n, combinacion);
	}
	
	public static int pedirM() {
		int m;
		do {
			Scanner entrada = new Scanner(System.in);
			System.out.println("Introduce un número m mayor o igual que 0");
			m = entrada.nextInt();
		}while(m < 0);
		return m;
	}
	
	public static int pedirN(int m) {
		int n;
		do {
			Scanner entrada = new Scanner(System.in);
			System.out.println("Introduce un número n mayor o igual que 0");
			n = entrada.nextInt();
		}while(n < 0 || m<n);
		return n;
	}
	public static int calcularFactorial(int numero){
		if(numero == 0) {
			return 1;
		}else {
			return numero * calcularFactorial(numero-1);
		}
	}
	
	
	public static int calcularCombinacion(int m, int n) {
		return calcularFactorial(m)/(calcularFactorial(n)*calcularFactorial(m-n));
	}
	
	public static void verResultado(int m, int n, int combinacion) {
		System.out.println("La combinación es " + combinacion);

	}
	

}
