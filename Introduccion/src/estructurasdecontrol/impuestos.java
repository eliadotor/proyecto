package estructurasdecontrol;

import java.util.Scanner;

public class impuestos {

	public static void main(String[] args) {
		// Preguntar por teclado renta anual
		
		Scanner entrada = new Scanner(System.in);
		
		System.out.println("Introduzca su renta anual");
		int renta = entrada.nextInt();
		int impositivo = 0;
		if(renta <= 0) {
			System.out.println("La renta tiene que ser mayor que 0");
		}else {
			if(renta < 10000){
				impositivo = 5;
				}else if(renta < 20000){
					impositivo = 15;
					}else { 
						if(renta < 35000){
							impositivo = 20;
						}else {
							if(renta < 45000){
								impositivo = 30;
							}else {
								impositivo = 45;
							}
						}
					}
		System.out.println("Su valor impositivo es " + impositivo + "%");
		}
	}

}
