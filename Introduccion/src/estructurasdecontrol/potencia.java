package estructurasdecontrol;

import java.util.Scanner;

public class potencia {

	public static void main(String[] args) {
		
		int resultado = 1;
		//Introducimos la base y el exponente
		Scanner entrada = new Scanner(System.in);
		System.out.println("Introduzca la base");
		int base = entrada.nextInt();
		System.out.println("Introduzca el exponente");
		int exp = entrada.nextInt();
		//calculamos la potencia
		for(int i=1; i<=exp; i++) {
			resultado = resultado*base;
		}
		//Enviamos a pantalla el resultado
		System.out.println("El resultado es " + resultado);
	}

}
