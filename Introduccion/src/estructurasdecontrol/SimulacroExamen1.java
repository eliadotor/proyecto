package estructurasdecontrol;

import java.util.Scanner;

public class SimulacroExamen1 {

	public static void main(String[] args) {
		int clave = 1234;
		boolean pinValido;
		int intentos = 3;
		boolean salir;
		
		Scanner entrada = new Scanner (System.in);
		do {
			System.out.println("Introduce el pin");
			int pin = entrada.nextInt();
			pinValido = (pin==1234);
			salir = (intentos==0);
			if(!pinValido) {
				intentos--;
				System.out.println("Le quedan" + intentos + "intentos");
			}else {
				salir = true;
			}
			
			
		}while(!salir && intentos > 0);
		if(!salir) {
			System.out.println("Ha superado el número de intentos");
		}else {
			System.out.println("El pin es correcto");

		}

	}

}
