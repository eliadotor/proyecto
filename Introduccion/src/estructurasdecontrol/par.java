package estructurasdecontrol;

//Comprobar si el número introducido por teclado es par

import java.util.Scanner;

public class par {

	public static void main(String[] args) {
			
		Scanner entrada = new Scanner(System.in);
		//introducir un número por teclado
		System.out.println("Introduce un número");
		int num = entrada.nextInt();
		
		if(num % 2 == 0){
			System.out.println("El número " + num + " es par");
			
			}else {
				System.out.println("El número " + num + " no es par");
			}
	}

}
