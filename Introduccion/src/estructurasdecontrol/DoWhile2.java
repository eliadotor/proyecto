/*Introduce por teclado una respuesta valida
 * la respuesta es valida solo cuando sea una 
 * s minuscula o una n minuscula.
 */
package estructurasdecontrol;

import java.io.IOException;

public class DoWhile2 {

	public static void main(String[] args) throws IOException {
		
		boolean respuestaValida = true;
		
		// TODO Auto-generated method stub
		System.out.println("Introduce la respuesta:");
		char respuesta;

		do {
			respuesta = (char)System.in.read();
			//convertimos a minusculas
			respuesta = Character.toLowerCase(respuesta);
			//Limpiamos el buffer de teclado
			System.in.skip(2);
			//Comprobamos si la respuesta es valida
			
			respuestaValida = (respuesta  == 's' || respuesta == 'n');
			
			if(!respuestaValida) {
				System.out.println("error: teclea una s o una n");
			}
			
		}while(!respuestaValida);
		
		//respuesta no valida
		System.out.println("fin de programa");
	}

}
