/*
 * Este programa visualiza el máximo
 * común divisor de dos números
 */

package estructurasdecontrol;

import java.util.Scanner;

public class Mcd {

	public static void main(String[] args) {
		//Inrtroducimos por teclado los numeros
		Scanner entrada = new Scanner(System.in);
		System.out.println("Introduzca el número1");
		int num1 = entrada.nextInt();
		System.out.println("Introduzca el número2");
		int num2 = entrada.nextInt();
		//comparamos número 1 con número 2.
		int resultado = 0;
		
		while( num1 != num2) {
			if(num1 > num2) {
				num1 = num1 - num2;
			}else {
				num2 = num2 - num1;
			}
		}
		System.out.println("El MCD es " + num1);

	}

}
