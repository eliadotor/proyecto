/*Introduce por teclado una respuesta valida
 * la respuesta es valida solo cuando sea una 
 * s minuscula o una n minuscula.
 */
package estructurasdecontrol;

import java.io.IOException;

public class DoWhile1 {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		System.out.println("Introduce la respuesta:");
		char respuesta;

		do {
			respuesta = (char)System.in.read();
			respuesta = Character.toLowerCase(respuesta);
			
			//Limpiamos el buffer de teclado
			System.in.skip(2);
			if(respuesta != 's' && respuesta != 'n') {
				System.out.println("error: teclea una s o una n");
			}
			
		}while(respuesta != 's' && respuesta != 'n');
		
		//respuesta no valida
		System.out.println("fin de programa");
	}

}
