package estructurasdecontrol;

public class forAbecedario {

	public static void main(String[] args) {
		int tamAbc = 'z' - 'a' +1;
		
		for (char letra ='a'; letra<='z'; letra++ ) {
			System.out.print(letra + "   ");
		}
		System.out.println("\n\n");
		for (int i = 'a'; i<='z'; i++) {
			System.out.print(i + "  ");
			//System.out.print((char)i + "  "); convertirlo directamente a letra como en el for anterior
		}
		System.out.println("\n\n");
		int tamaAbc = 'Z' - 'A' +1;
		
		for (char letra ='A'; letra<='Z'; letra++ ) {
			System.out.print(letra + "   ");
		}
		System.out.println("\n\n");
		for (int i = 'A'; i<='Z'; i++) {
			System.out.print(i + "  ");
			//System.out.print((char)i + "  "); convertirlo directamente a letra como en el for anterior
		}

	}

}
