/*Introduce por teclado un conjunto de notas
 * hasta que te crea el flag -99
 * visualizando en pantalla la nota media.
 * La nota es valida si esta comprendida 
 * entre 1 y 10, en caso contrario deberá 
 * indicar que no es valida la nota 
 * y volver a pedirla
 */

package estructurasdecontrol;

import java.util.Scanner;

public class MediaNota {

	public static void main(String[] args) {
		
		boolean salir = false; //variable que controla la salida de notas
		int contador = 0;
		float sumaNotas = 0;
		float nota = 0;
		
		Scanner entrada = new Scanner(System.in);
		
		//pedimos la nota
		System.out.println("Introduce tu nota, tecle -99 para salir");
		
		do {
			//almacenamos la nota
			nota = entrada.nextFloat();
			/*do {
				
				if(nota < 0 || nota > 10) {
					System.out.println("La nota no es valida, tiene que estar comprendida entre 1 y 10. \n Introduzca otra nota.");

				}
			}while(nota < 0 || nota > 10);*/
			
			
			//comprobamos si ha introducido un -99
			
			/*if(nota == -99) {
				salir = true;
			}*/
			//Lo mismo que lo anterior seria
			
			salir = (nota == -99);
			boolean notaValida = (nota > 0 && nota <= 10);
			
			
			if(!salir && notaValida) {
				sumaNotas = sumaNotas + nota;
				contador ++; 
			}else if(!salir) {
				System.out.println("La nota no es valida, tiene que estar comprendida entre 1 y 10. \n Introduzca otra nota.");

			}
			
		}while(!salir);
		
		//Calculamos la nota media
		
		if(contador > 0) {
			float notaMedia = sumaNotas /contador;
			System.out.println("La nota media es " + notaMedia);
		}else {
			System.out.println("No ha introducido ninguna nota");
			System.out.println("Fin de programa");
		}
		
		
		
		

	}

}
