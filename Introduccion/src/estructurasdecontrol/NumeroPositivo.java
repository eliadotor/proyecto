package estructurasdecontrol;

import java.util.Scanner;

public class NumeroPositivo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Creamos un objerto de tipo Scanner
		Scanner entrada = new Scanner(System.in);
		
		//Introducimos un numero por pantalla
		System.out.println("Introduce un número");
		int num = entrada.nextInt();
		
		String mensaje= "El número "; 
		
		
		if (num > 0) {
			System.out.println(mensaje + num + " es positivo");
				}else if(num<0) {
					System.out.println(mensaje + num + " es negativo");
					}else {
						System.out.println("El número es 0");
						}
		

	}

}
