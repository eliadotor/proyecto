package estructurasdecontrol;

import java.util.Scanner;

public class LlamadaConstante {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		boolean duracionValida = true;
		final int COSTE_MINIMO = 20;
		final int COSTE_MINUTO_ADICIONAL = 15;
		final int DURACION_MINIMA = 3;
		int minutoAdicional = 15;
		int duracion = 0;
		int precioLlamada = 0;
		
		System.out.println("Introduzca la duracion de tu llamada");
		
		
		Scanner entrada = new Scanner(System.in);
		
		//Validamos la duracion
		do {
			duracion = entrada.nextInt();
			duracionValida = duracion > 0;
			if(!duracionValida) {
				System.out.println("La duración de la llamada no es valida, debe ser mayor que 0");
			}
		}while(!duracionValida);
		
		if(duracion <= DURACION_MINIMA) {
			precioLlamada = COSTE_MINIMO;
		}else{
			duracion = duracion - DURACION_MINIMA;
			precioLlamada = COSTE_MINIMO + duracion*COSTE_MINUTO_ADICIONAL;
		}
		System.out.println("El precio de la llamada es " + precioLlamada + " ptas.");
		
	}

}
