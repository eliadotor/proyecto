package estructurasdecontrol;

public class TablasMultiplicar {

	public static void main(String[] args) {
		
		for(int j=1; j<=10; j++) {
			//System.out.print("Tabla del " + j + ":");
			System.out.println("Tabla del " + j + ":");
			for(int i = 1; i<=10; i++) {
				System.out.println(j + "x" + i + "=" + j * i);
				//System.out.print(j + "x" + i + "=" + (i*j) + "\t");
			}//fin for anidado
			System.out.println();
		}//fin for
		
	}//fin main
}//fin public
