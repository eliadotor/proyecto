package estructurasdecontrol;

import java.util.Scanner;

public class notas {

	public static void main(String[] args) {
		//vARIABLES QUE NECISO PARA ALMACENAR
		int nota;
		int notaMax = 0;
		int notaMin = 10;
		boolean notaValida = false;
		System.out.println("Introduce tu nota:"); //pedir al usuario que introduzca la nota
		Scanner entrada = new Scanner(System.in); //sentencia de entrada de datos
		
		for(int i=1 ; i<= 3; i++){
			do {
				//guardamos la nota y la validamos
				nota = entrada.nextInt();
				notaValida = (nota >= 1 && nota <= 10);
				if(!notaValida){
					System.out.println("La nota que has introducido no es valida, tiene que estar introducida entre 1 y 10.\nVuelve a introducir tu nota:");
				}
			}while(!notaValida);
			//comparamos notas
			if(nota > notaMax){
				notaMax = nota;
			}
			if(nota < notaMin) {
				notaMin = nota;
			}
		}
		System.out.println("La nota máxima es " + notaMax + " y la nota mínima es " + notaMin);
	}

}
