package estructurasdecontrol;

//Importamos la clase Scanner
import java.util.Scanner;
class SumaNumeros
{
    /*ESte programa introduce por teclado 
      dos números y visualiza en pantalla 
      su suma*/

    public static void main(String args[])
    {
        //Creamos un objeto de tipo escaner
        Scanner entrada = new Scanner(System.in);
        
        //Introducimos por teclado los números
        System.out.println("Introduce número 1");
        int num1 = entrada.nextInt();
        System.out.println("Introduce número 2");
        int num2 = entrada.nextInt();

        //Calculamos la suma
         int suma= num1 + num2;

        //Enviamos a pantalla el resultado
         System.out.println("La suma es " + suma);

      }

}
