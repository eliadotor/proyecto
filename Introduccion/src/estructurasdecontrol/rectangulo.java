package estructurasdecontrol;

//Importamos la clase Scanner
import java.util.Scanner;
public class rectangulo
{
    /*Este programa introduce por teclado 
      la base y la altura de un rectángulo
	y visualiza en pantalla 
      su area y su perimetro*/

    public static void main(String args[])
    {
        //Creamos un objeto de tipo escaner
        Scanner entrada = new Scanner(System.in);
        
        //Introducimos por teclado la base del rectángulo
        System.out.println("Introduce la base del rectángulo");
        int base = entrada.nextInt();
        System.out.println("Introduce la altura del rectángulo");
        int altura = entrada.nextInt();

        //Calculamos el area
         int area= base + altura;

        //Enviamos a pantalla el resultado
         System.out.println("El área del rectángulo es " + area);

	 //Calculamos el perimetro
         int perimetro= 2*(base + altura);

        //Enviamos a pantalla el resultado
         System.out.println("El perimetro del rectángulo es " + perimetro);

      }

}
