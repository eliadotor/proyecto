package estructurasdecontrol;

import java.util.Scanner;

public class ConjuntoNumeros {

	public static void main(String[] args) {
		
				int contador15 = 0;
				int contador55 = 0;
				int contadorEntre = 0;
				Scanner entrada = new Scanner(System.in);
				System.out.println("Introduce seis números");
				//Hacemos un bucle for
				for(int i= 1; i<=6; i++) {
					int num = entrada.nextInt();
					if(num < 15) {
						contador15 ++;
						}else if(num > 55) {
							contador55 ++;
						}else {
							if(num >= 45) {
								contadorEntre ++;
							}
					}
				}
					System.out.println("En este conjunto de 6 numeros tenemos " + contador15 + " números que son menores que 15, " + contador55 + " números que son mayores que 55 y " + contadorEntre + " números comprendidos entre 45 y 55.");
	}

}
